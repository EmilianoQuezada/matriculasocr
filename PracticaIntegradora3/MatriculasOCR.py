import cv2
import pytesseract
pytesseract.pytesseract.tesseract_cmd = r'E:\Tessereact\tesseract'


def determinarEstado(placa):
  serie = placa[:3]
  if "AAA" <= placa <= "AFZ":
    return "Aguascalientes"
  elif "AGA" <= placa <= "CYZ":
    return "Baja California"
  elif "CZA" <= placa <= "DEZ":
    return "Baja California Sur"
  elif "DFA" <= placa <= "DKZ":
    return "Campeche"
  elif "DLA" <= placa <= "DSZ":
    return "Chiapas"
  elif "DTA" <= placa <= "ETZ":
    return "Chihuahua"
  elif "EUA" <= placa <= "FPZ":
    return "Coahulia"
  elif "FRA" <= placa <= "FWZ":
    return "Colima"
  elif "FXA" <= placa <= "GFZ":
    return "Durango"
  elif "LGA" <= placa <= "PEZ":
    return "Estado de Mexico"
  elif "GGA" <= placa <= "GYZ":
    return "Guanajuato"
  elif "GZA" <= placa <= "HFZ":
    return "Guerrero"
  elif "HGA" <= placa <= "HRZ":
    return "Hidalgo"
  elif "HSA" <= placa <= "LFZ":
    return "Sinaloa"
  elif "PFA" <= placa <= "PUZ":
    return "Jalisco"
  elif "PVA" <= placa <= "RDZ":
    return "Michoacan"
  elif "REA" <= placa <= "RJZ":
    return "Morelos"
  elif "RKA" <= placa <= "TGZ":
    return "Nayarit"
  elif "THA" <= placa <= "TMZ":
    return "Nuevo Leon"
  elif "TNA" <= placa <= "UJZ":
    return "OAXACA"
  elif "UKA" <= placa <= "UPZ":
    return "Puebla"
  elif "URA" <= placa <= "UVZ":
    return "Queretaro"
  elif "UWA" <= placa <= "VEZ":
    return "Quintana Roo"
  elif "VFA" <= placa <= "VSZ":
    return "San Luis Potosi"
  elif "VFA" <= placa <= "VSZ":
    return "Sinaloa"
  elif "VTA" <= placa <= "WKZ":
    return "Sonora"
  elif "WLA" <= placa <= "WWZ":
    return "Tabasco"
  elif "WXA" <= placa <= "XSZ":
    return "Tamaulipas"
  elif "XTA" <= placa <= "XXZ":
    return "Tlaxcala"
  elif "XYA" <= placa <= "YVZ":
    return "Veracruz"
  elif "YWA" <= placa <= "ZCZ":
    return "Yucatan"
  elif "ZDA" <= placa <= "ZHZ":
    return "Zacatecas"
  else:
        return "Estado no valido"


placa = []

image = cv2.imread(r'C:\Users\user\Desktop\OCR Pytesseract\PracticaIntegradora3\auto001.jpg')
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = cv2.blur(gray,(3,3))
canny = cv2.Canny(gray,150,200)
canny = cv2.dilate(canny,None,iterations=1)

#_,cnts,_ = cv2.findContours(canny,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
cnts,_ = cv2.findContours(canny,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
#cv2.drawContours(image,cnts,-1,(0,255,0),2)

for c in cnts:
  area = cv2.contourArea(c)

  x,y,w,h = cv2.boundingRect(c)
  epsilon = 0.09*cv2.arcLength(c,True)
  approx = cv2.approxPolyDP(c,epsilon,True)
  
  if len(approx)==4 and area>9000:
    print('area=',area)

    aspect_ratio = float(w)/h
    if aspect_ratio>0.5:
      placa = gray[y:y+h,x:x+w]
      text = pytesseract.image_to_string(placa,config='--psm 11')
      print('PLACA: ',text)
      estado = determinarEstado(text)
      print('Estado:',estado)
      cv2.imshow('PLACA',placa)
      cv2.moveWindow('PLACA',780,10)
      cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),3)
  
cv2.imshow('Image',image)
cv2.moveWindow('Image',45,10)
cv2.waitKey(0)


